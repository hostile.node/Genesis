# Genesis

Game engine developed in C++.

# Dependencies

## Linux

apt install libglew-dev libsdl2-image-dev

# Credits

Project icon by [Freepik](https://www.freepik.com) from [FlatIcon](https://www.flaticon.com/), licensed by [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/).
