// Copyright 2015 Pedro Nunes
//
// This file is part of Genesis.
//
// Genesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Genesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Genesis. If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>
#include <chrono>
#include <random>

#include <fmod.h>
#include <fmod.hpp>
#include <fmod_errors.h>

#include "configuration.h"
#include "genesis.h"
#include "logger.h"
#include "resources/resourcesound.h"

#include "soundmanager.h"

namespace Genesis
{

static void CheckFMODResult( FMOD_RESULT result )
{
    if ( result != FMOD_OK )
    {
        Genesis::FrameWork::GetLogger()->LogWarning( "FMOD error %d: %s", result, FMOD_ErrorString( result ) );
    }
}

void ToFMODVector( const glm::vec3& inVector, FMOD_VECTOR* pOutVector )
{
    pOutVector->x = inVector.x;
    pOutVector->y = inVector.y;
    pOutVector->z = inVector.z;
}

///////////////////////////////////////////////////////////////////////////////
// SoundInstance
///////////////////////////////////////////////////////////////////////////////

SoundInstance::SoundInstance()
    : m_pResource( nullptr )
    , m_pChannel( nullptr )
    , m_PlaybackPending( false )
{
}

SoundInstance::~SoundInstance()
{
    if ( IsPlaying() )
    {
        Stop();
    }
}

void SoundInstance::Initialise( ResourceSound* pResourceSound, void* pData )
{
    m_pResource = pResourceSound;
    m_pChannel = (FMOD::Channel*)pData;

    // By default, a 3D sound is placed by FMOD at the same position as the listener, which is almost certainly
    // not where we want it to be. As such, 3D sounds start paused and are unpaused when Set3DAttributes is called.
    m_PlaybackPending = pResourceSound->Is3D();

	// Calling SetVolume when the sound is initialised ensures that the user's volume levels are respected.
	SetVolume( 1.0f );
}

void SoundInstance::Set3DAttributes( const glm::vec3* pPosition /* = nullptr */, const glm::vec3* pVelocity /* = nullptr */ )
{
    if ( m_pResource == nullptr )
    {
        FrameWork::GetLogger()->LogInfo( "Can't call SoundInstance::Set3DAttributes, no resource set, sound not initialised?" );
        return;
    }
    else if ( m_pResource->Is3D() == false )
    {
        FrameWork::GetLogger()->LogInfo( "Can't call Set3DAttributes on sound '%s', resource not set as 3D.", m_pResource->GetFilename().GetFullPath().c_str() );
        return;
    }

    FMOD_VECTOR fmodPosition = { 0.0f, 0.0f, 0.0f };
    FMOD_VECTOR fmodVelocity = { 0.0f, 0.0f, 0.0f };

    if ( pPosition != nullptr )
        ToFMODVector( *pPosition, &fmodPosition );
    if ( pVelocity != nullptr )
        ToFMODVector( *pVelocity, &fmodVelocity );

    FMOD_RESULT result = m_pChannel->set3DAttributes( pPosition ? &fmodPosition : nullptr, pVelocity ? &fmodVelocity : nullptr );
    CheckFMODResult( result );

    if ( m_PlaybackPending && m_pChannel != nullptr )
    {
        m_pChannel->setPaused( false );
    }
}

void SoundInstance::Get3DAttributes( glm::vec3* pPosition /* = nullptr */, glm::vec3* pVelocity /* = nullptr */ )
{
    FMOD_VECTOR position = { 0.0f, 0.0f, 0.0f };
    FMOD_VECTOR velocity = { 0.0f, 0.0f, 0.0f };
    m_pChannel->get3DAttributes( pPosition ? &position : nullptr, pVelocity ? &velocity : nullptr );

    if ( pPosition != nullptr )
    {
        pPosition->x = position.x;
        pPosition->y = position.y;
        pPosition->z = position.z;
    }

    if ( pVelocity != nullptr )
    {
        pVelocity->x = velocity.x;
        pVelocity->y = velocity.y;
        pVelocity->z = velocity.z;
    }
}

void SoundInstance::SetMinimumDistance( float value )
{
    if ( m_pResource == nullptr )
    {
        FrameWork::GetLogger()->LogInfo( "Can't call SoundInstance::SetMinimumDistance, no resource set, sound not initialised?" );
        return;
    }
    else if ( m_pResource->Is3D() == false )
    {
        FrameWork::GetLogger()->LogInfo( "Can't call SoundInstance::SetMinimumDistance on sound '%s', resource not set as 3D.", m_pResource->GetFilename().GetFullPath().c_str() );
        return;
    }

    FMOD_RESULT result = m_pChannel->set3DMinMaxDistance( value, 10000.0f );
    CheckFMODResult( result );
}

bool SoundInstance::IsPlaying() const
{
    bool isPlaying = false;

    if ( m_pChannel != nullptr )
    {
        m_pChannel->isPlaying( &isPlaying );
    }

    return isPlaying;
}

void SoundInstance::Stop()
{
    if ( IsPlaying() )
    {
        m_pChannel->stop();
    }
}

ResourceSound* SoundInstance::GetResource() const
{
    return m_pResource;
}

unsigned int SoundInstance::GetLength() const
{
    if ( m_pResource )
    {
        unsigned int length;
        m_pResource->GetSound()->getLength( &length, FMOD_TIMEUNIT_MS );
        return length;
    }
    else
    {
        return 0;
    }
}

unsigned int SoundInstance::GetPosition() const
{
    if ( m_pChannel )
    {
        unsigned int position;
        m_pChannel->getPosition( &position, FMOD_TIMEUNIT_MS );
        return position;
    }
    else
    {
        return 0;
    }
}

float SoundInstance::GetAudability() const
{
    if ( m_pChannel && IsPlaying() )
    {
        float audability = 0.0f;
        CheckFMODResult( m_pChannel->getAudibility( &audability ) );
        return audability;
    }
    else
    {
        return 0.0f;
    }
}

void SoundInstance::SetVolume( float value )
{
    if ( m_pChannel )
    {
		// Values from Configuration are [0u-100u], need to be remapped to [0.0f-1.0f]
		value *= static_cast<float>( Configuration::GetMasterVolume() ) / 100.0f;
		if ( m_pResource->IsEffect() )
		{
			value *= static_cast<float>( Configuration::GetSFXVolume() ) / 100.0f;
		}
		else
		{
			value *= static_cast<float>( Configuration::GetMusicVolume() ) / 100.0f;
		}

        m_pChannel->setVolume( value );
    }
}

float SoundInstance::GetVolume() const
{
    float volume = 0.0f;
    if ( IsPlaying() )
    {
        CheckFMODResult( m_pChannel->getVolume( &volume ) );
    }
    return volume;
}

///////////////////////////////////////////////////////////////////////////////
// SoundManager
///////////////////////////////////////////////////////////////////////////////

SoundManager::SoundManager()
    : m_pSystem( nullptr )
    , m_pActivePlaylist( nullptr )
    , m_PlaylistSoundIndex( -1 )
    , m_Timer( 0.0f )
{
    m_Seed = static_cast<unsigned int>( std::chrono::system_clock::now().time_since_epoch().count() );

    FMOD_RESULT result;
    result = FMOD::System_Create( &m_pSystem );

    unsigned int version;
    result = m_pSystem->getVersion( &version );
    CheckFMODResult( result );

    if ( version < FMOD_VERSION )
    {
        FrameWork::GetLogger()->LogError( "You are using an old version of FMOD %08x. This program requires %08x\n", version, FMOD_VERSION );
        return;
    }

    int numdrivers;
    FMOD_SPEAKERMODE speakermode;
    FMOD_CAPS caps;
    char name[ 256 ];

    result = m_pSystem->getNumDrivers( &numdrivers );
    CheckFMODResult( result );

    if ( numdrivers == 0 )
    {
        result = m_pSystem->setOutput( FMOD_OUTPUTTYPE_NOSOUND );
        CheckFMODResult( result );
    }
    else
    {
        result = m_pSystem->getDriverCaps( 0, &caps, 0, &speakermode );
        CheckFMODResult( result );

        result = m_pSystem->setSpeakerMode( speakermode ); /* Set the user selected speaker mode. */
        CheckFMODResult( result );

        if ( caps & FMOD_CAPS_HARDWARE_EMULATED ) /* The user has the 'Acceleration' slider set to off!  This is really bad for latency!. */
        { /* You might want to warn the user about this. */
            result = m_pSystem->setDSPBufferSize( 1024, 10 );
            CheckFMODResult( result );
        }

        result = m_pSystem->getDriverInfo( 0, name, 256, 0 );
        CheckFMODResult( result );

        if ( strstr( name, "SigmaTel" ) ) /* Sigmatel sound devices crackle for some reason if the format is PCM 16bit.  PCM floating point output seems to solve it. */
        {
            result = m_pSystem->setSoftwareFormat( 48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0, 0, FMOD_DSP_RESAMPLER_LINEAR );
            CheckFMODResult( result );
        }
    }

    result = m_pSystem->init( 100, FMOD_INIT_NORMAL, nullptr ); // Initialize FMOD with 100 virtual voices.
    CheckFMODResult( result );

    result = m_pSystem->set3DSettings( 1.0f, 1.0f, 1.0f );
    CheckFMODResult( result );
}

SoundManager::~SoundManager()
{
    CheckFMODResult( m_pSystem->close() );
    CheckFMODResult( m_pSystem->release() );
}

TaskStatus SoundManager::Update( float delta )
{
    m_Timer += delta;

    m_pSystem->update();

    if ( m_pActivePlaylist && m_pPlaylistSoundInstance && !m_pPlaylistSoundInstance->IsPlaying() )
    {
        PlayNextTrack();
    }

    // Clear any instances which are no longer being references externally
    for ( SoundInstanceList::iterator it = m_SoundInstances.begin(); it != m_SoundInstances.end(); )
    {
        if ( it->use_count() == 1 && ( *it )->IsPlaying() == false )
        {
            it = m_SoundInstances.erase( it );
        }
        else
        {
            ++it;
        }
    }

	// Force the update of the music's volume to guarantee that it reflects the settings as the player
	// modifies them.
	if ( m_pPlaylistSoundInstance && m_pPlaylistSoundInstance->IsPlaying() )
	{
		m_pPlaylistSoundInstance->SetVolume( 1.0f );
	}

    return TaskStatus::Continue;
}

FMOD::Sound* SoundManager::CreateSound( ResourceSound* pResourceSound )
{
    unsigned int flags = FMOD_DEFAULT;

    if ( pResourceSound->IsPlaylist() == false )
    {
        if ( !pResourceSound->IsEffect() && !pResourceSound->IsStreamed() )
        {
            FrameWork::GetLogger()->LogWarning( "SoundManager::CreateSound ('%s'): Unable to create sound, resource not set as streaming or effect.", pResourceSound->GetFilename().GetFullPath().c_str() );
            return nullptr;
        }

        if ( pResourceSound->IsHardwareMixed() )
        {
            flags |= FMOD_HARDWARE;
        }
        else if ( pResourceSound->IsSoftwareMixed() )
        {
            flags |= FMOD_SOFTWARE;
        }

        if ( pResourceSound->IsLooping() )
        {
            flags |= FMOD_LOOP_NORMAL;
        }

        if ( pResourceSound->Is3D() )
        {
            flags |= FMOD_3D;
        }
    }

    FMOD::Sound* pSound = nullptr;
    if ( pResourceSound->IsStreamed() )
    {
        FMOD_RESULT result = m_pSystem->createStream( pResourceSound->GetFilename().GetFullPath().c_str(), flags, nullptr, &pSound );
        CheckFMODResult( result );
    }
    else
    {
        FMOD_RESULT result = m_pSystem->createSound( pResourceSound->GetFilename().GetFullPath().c_str(), flags, nullptr, &pSound );
        CheckFMODResult( result );
    }

    return pSound;
}

SoundInstanceSharedPtr SoundManager::CreateSoundInstance( ResourceSound* pResourceSound )
{
    FMOD::Sound* pSound = pResourceSound->GetSound();

    if ( pSound == nullptr )
    {
        FrameWork::GetLogger()->LogWarning( "SoundManager::CreateSoundInstance ('%s'): Attempted to create sound instance from invalid sound", pResourceSound->GetFilename().GetFullPath().c_str() );
        return nullptr;
    }
    else if ( UpdateInstancingLimit( pResourceSound ) )
    {
        // 3D sounds start paused - they'll be unpaused once the 3D attributes are set
        FMOD::Channel* pChannel = nullptr;
        FMOD_RESULT result = m_pSystem->playSound( FMOD_CHANNEL_FREE, pSound, pResourceSound->Is3D(), &pChannel );
        CheckFMODResult( result );

        if ( pChannel != nullptr )
        {
            SoundInstanceSharedPtr soundInstance = std::make_shared<SoundInstance>();
            soundInstance->Initialise( pResourceSound, pChannel );
            m_SoundInstances.push_back( soundInstance );
            return m_SoundInstances.back();
        }
    }

    return nullptr;
}

void SoundManager::SetPlaylist( ResourceSound* pResourceSound, const std::string& startingTrack /* = "" */, bool shuffle /* = false */ )
{
    if ( pResourceSound == m_pActivePlaylist )
    {
        return;
    }

    if ( pResourceSound->IsPlaylist() == false )
    {
        FrameWork::GetLogger()->LogWarning( "SetPlaylist() failed, '%s' is not a valid playlist resource", pResourceSound->GetFilename().GetFullPath().c_str() );
        return;
    }
    else
    {
        if ( m_pActivePlaylist != nullptr && m_pPlaylistSoundInstance != nullptr )
        {
            m_pPlaylistSoundInstance->Stop();
        }

        m_pActivePlaylist = pResourceSound;
        m_pActivePlaylist->Initialise();

        CacheTracks( shuffle );

        if ( m_CachedTracks.empty() == false )
        {
            if ( startingTrack.empty() )
            {
                PlayNextTrack();
            }
            else
            {
                PlayTrack( startingTrack );
            }
        }
    }
}

void SoundManager::CacheTracks( bool shuffle /* = false */ )
{
    m_PlaylistSoundIndex = -1;
    m_CachedTracks.clear();
    if ( m_pActivePlaylist == nullptr )
        return;

    FMOD::Sound* pPlaylistSound = m_pActivePlaylist->GetSound();
    int numSongs = 0;
    FMOD_RESULT result = pPlaylistSound->getNumTags( &numSongs, nullptr );
    CheckFMODResult( result );

    for ( int i = 0; i < numSongs; ++i )
    {
        FMOD_TAG tag;
        result = pPlaylistSound->getTag( "FILE", i, &tag );
        CheckFMODResult( result );

        if ( result == FMOD_OK )
        {
            std::string trackName( (char*)tag.data );
            std::string fullPath = m_pActivePlaylist->GetFilename().GetDirectory() + trackName;
            ResourceSound* pTrackResource = (ResourceSound*)FrameWork::GetResourceManager()->GetResource( fullPath );
            if ( pTrackResource != nullptr )
            {
                m_CachedTracks.push_back( pTrackResource );
            }
        }
    }

    if ( shuffle && m_CachedTracks.size() > 1 )
    {
        std::shuffle( m_CachedTracks.begin(), m_CachedTracks.end(), std::default_random_engine( m_Seed ) );
    }
}

bool SoundManager::HasValidPlaylist() const
{
    if ( m_pActivePlaylist == nullptr || m_pActivePlaylist->GetSound() == nullptr )
    {
        return false;
    }

    int numTracks = m_CachedTracks.size();
    if ( numTracks == 0 )
    {
        FrameWork::GetLogger()->LogInfo( "Playlist '%s' has no tracks in it", m_pActivePlaylist->GetFilename().GetName().c_str() );
        return false;
    }

    return true;
}

bool SoundManager::PlayNextTrack()
{
    if ( HasValidPlaylist() == false )
        return false;

    const int numTracks = m_CachedTracks.size();
    const int trackToPlay = ( ++m_PlaylistSoundIndex ) % numTracks;
    ResourceSound* pNextTrackResource = m_CachedTracks[ trackToPlay ];

    FrameWork::GetLogger()->LogInfo( "Next track in queue (%d / %d): '%s'", trackToPlay + 1, numTracks, pNextTrackResource->GetFilename().GetName().c_str() );

    pNextTrackResource->Initialise( SOUND_FLAG_STREAM );
    if ( pNextTrackResource->IsValid() )
    {
        m_pPlaylistSoundInstance = CreateSoundInstance( pNextTrackResource );
        m_PlaylistSoundIndex = trackToPlay;
        return true;
    }
    else
    {
        m_PlaylistSoundIndex = -1;
        return false;
    }
}

bool SoundManager::PlayTrack( const std::string& trackName )
{
    m_PlaylistSoundIndex = -1;

    if ( HasValidPlaylist() == false )
        return false;

    const int numTracks = m_CachedTracks.size();
    for ( int i = 0; i < numTracks; ++i )
    {
        ResourceSound* pTrackResource = m_CachedTracks[ i ];

        if ( trackName == pTrackResource->GetFilename().GetName() )
        {
            pTrackResource->Initialise( SOUND_FLAG_STREAM );
            if ( pTrackResource->IsValid() )
            {
                m_pPlaylistSoundInstance = CreateSoundInstance( pTrackResource );
                m_PlaylistSoundIndex = i;
                return true;
            }
        }
    }

    return false;
}

void SoundManager::SetListener( const glm::vec3& position, const glm::vec3& velocity, const glm::vec3& forward, const glm::vec3& up )
{
    FMOD_VECTOR fmodPosition;
    FMOD_VECTOR fmodVelocity;
    FMOD_VECTOR fmodForward;
    FMOD_VECTOR fmodUp;

    ToFMODVector( position, &fmodPosition );
    ToFMODVector( velocity, &fmodVelocity );
    ToFMODVector( forward, &fmodForward );
    ToFMODVector( up, &fmodUp );

    m_pSystem->set3DListenerAttributes(
        0,
        &fmodPosition,
        &fmodVelocity,
        0,
        0 );
}

glm::vec3 SoundManager::GetListenerPosition() const
{
    FMOD_VECTOR fmodPosition;
    m_pSystem->get3DListenerAttributes( 0, &fmodPosition, nullptr, nullptr, nullptr );
    return glm::vec3( fmodPosition.x, fmodPosition.y, fmodPosition.z );
}

int SoundManager::GetActiveSoundCount() const
{
    int count = 0;
    m_pSystem->getChannelsPlaying( &count );
    return count;
}

bool SoundManager::UpdateInstancingLimit( ResourceSound* pResourceSound )
{
    if ( pResourceSound->GetInstancingLimit() <= 0.0f )
        return true;

    for ( auto& instanceLimit : m_InstanceLimits )
    {
        if ( instanceLimit.m_pResourceSound == pResourceSound )
        {
            if ( instanceLimit.m_NextAllowedInstance > m_Timer )
            {
                return false;
            }
            else
            {
                instanceLimit.m_NextAllowedInstance = m_Timer + pResourceSound->GetInstancingLimit();
                return true;
            }
        }
    }

    InstanceLimit instanceLimit;
    instanceLimit.m_pResourceSound = pResourceSound;
    instanceLimit.m_NextAllowedInstance = m_Timer + pResourceSound->GetInstancingLimit();
    m_InstanceLimits.push_back( instanceLimit );
    return true;
}
}