// Copyright 2015 Pedro Nunes
//
// This file is part of Genesis.
//
// Genesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Genesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Genesis. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <list>
#include <memory>

#include "taskmanager.h"

namespace FMOD
{

class Channel;
class Sound;
class System;
}

namespace Genesis
{

class ResourceSound;

class SoundInstance
{
public:
    SoundInstance();
    ~SoundInstance();

    void Initialise( ResourceSound* pResourceSound, void* pData );
    bool IsPlaying() const;
    void Stop();

    unsigned int GetLength() const; // Length in milliseconds
    unsigned int GetPosition() const; // Current position in milliseconds
    float GetAudability() const;

    ResourceSound* GetResource() const;

    void SetMinimumDistance( float value );
    void Set3DAttributes( const glm::vec3* pPosition = nullptr, const glm::vec3* pVelocity = nullptr );
    void Get3DAttributes( glm::vec3* pPosition = nullptr, glm::vec3* pVelocity = nullptr );
    void SetVolume( float value );
    float GetVolume() const;

private:
    ResourceSound* m_pResource;
    FMOD::Channel* m_pChannel;
    bool m_PlaybackPending;
};

typedef std::shared_ptr<SoundInstance> SoundInstanceSharedPtr;
typedef std::list<SoundInstanceSharedPtr> SoundInstanceList;
typedef std::vector<ResourceSound*> ResourceSoundVector;

class SoundManager : public Task
{
public:
    SoundManager();
    virtual ~SoundManager();

    TaskStatus Update( float delta );

    FMOD::Sound* CreateSound( ResourceSound* pResourceSound );
    SoundInstanceSharedPtr CreateSoundInstance( ResourceSound* pResourceSound );

    void SetPlaylist( ResourceSound* pResourceSound, const std::string& startingSong = "", bool shuffle = false );

    ResourceSound* GetPlaylistResource() const;
    SoundInstanceSharedPtr GetCurrentSong() const;

    const SoundInstanceList& GetSoundInstances() const;

    void SetListener( const glm::vec3& position, const glm::vec3& velocity, const glm::vec3& forward, const glm::vec3& up );
    glm::vec3 GetListenerPosition() const;

    int GetActiveSoundCount() const;

private:
    void CacheTracks( bool shuffle = false );
    bool PlayNextTrack();
    bool PlayTrack( const std::string& songName );
    bool HasValidPlaylist() const;
    bool UpdateInstancingLimit( ResourceSound* pResourceSound );

    FMOD::System* m_pSystem;
    SoundInstanceList m_SoundInstances;
    ResourceSound* m_pActivePlaylist;
    SoundInstanceSharedPtr m_pPlaylistSoundInstance;
    int m_PlaylistSoundIndex;
    ResourceSoundVector m_CachedTracks;

    struct InstanceLimit
    {
        ResourceSound* m_pResourceSound;
        float m_NextAllowedInstance;
    };
    typedef std::vector<InstanceLimit> InstanceLimitVector;
    InstanceLimitVector m_InstanceLimits;
    float m_Timer;
    unsigned int m_Seed;
};

inline ResourceSound* SoundManager::GetPlaylistResource() const
{
    return m_pActivePlaylist;
}

inline SoundInstanceSharedPtr SoundManager::GetCurrentSong() const
{
    return m_pPlaylistSoundInstance;
}

inline const SoundInstanceList& SoundManager::GetSoundInstances() const
{
    return m_SoundInstances;
}
}