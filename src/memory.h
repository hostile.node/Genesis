// Copyright 2016 Pedro Nunes
//
// This file is part of Genesis.
//
// Genesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Genesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Genesis. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <cstdint>
#include <string>
#include <list>

#include "SDL_mutex.h"

namespace Genesis
{

#ifndef _FINAL

// Memory allocators are tracked and kept in a list which is evaluated at when the MemoryAllocate is terminated,
// revealing memory leaks.
// A file called "memory.log" is created in the same folder as the .exe, containing the report.
#define GENESIS_ENABLE_MEMORY_TRACKER

// NOT IMPLEMENTED YET
// Adds an additional 4 bytes before and after an allocation. These bytes have a known value and can be evaluated at
// any point to check for buffer overruns / underruns, which should aid with tracking memory corruption issues.
#define GENESIS_ENABLE_MEMORY_CANARY

#endif

struct MemoryBlock
{
	void* pAddress;
	std::size_t size;
	std::string type;
	std::string file;
	unsigned int line;
};
typedef std::list< MemoryBlock > MemoryBlockList;

class MemoryAllocator
{
public:
	static void Initialise();
	static void Shutdown();

	static void* Allocate( std::size_t bytes, const char* pType, const char* pFilename, unsigned int line );

    template< typename T >
	static void Deallocate( T*& pAddress );

private:
	static MemoryBlockList m_Blocks;
	static SDL_mutex* m_pMemoryBlockMutex;
};

}

inline void* operator new( std::size_t bytes, const char* pType, const char* pFile, unsigned int line )
{
	void* pAddress = Genesis::MemoryAllocator::Allocate( bytes, pType, pFile, line );
	return pAddress;
}

inline void operator delete( void* pAddress, const char* pType, const char* pFile, unsigned int line )
{
	delete pAddress;
}


namespace Genesis
{

inline void* MemoryAllocator::Allocate( std::size_t bytes, const char* pType, const char* pFilename, unsigned int line )
{
	void* pAddress = new uint8_t[ bytes ];

#ifdef GENESIS_ENABLE_MEMORY_TRACKER
	MemoryBlock block;
	block.pAddress = pAddress;
	block.size = bytes;
	block.type = std::string( pType );
	block.file = std::string( pFilename );
	block.line = line;

	SDL_LockMutex( m_pMemoryBlockMutex );
	m_Blocks.emplace_back( block );
	SDL_UnlockMutex( m_pMemoryBlockMutex );
#endif

	return pAddress;
}

template< typename T >
void MemoryAllocator::Deallocate( T*& pAddress )
{
	if ( pAddress != nullptr )
	{
#ifdef GENESIS_ENABLE_MEMORY_TRACKER
		SDL_LockMutex( m_pMemoryBlockMutex );
		bool found = false;
		for ( MemoryBlockList::const_iterator it = m_Blocks.cbegin(); it != m_Blocks.cend(); ++it )
		{
			if ( (*it).pAddress == pAddress )
			{
				m_Blocks.erase( it );
				found = true;
				break;
			}
		}
		SDL_UnlockMutex( m_pMemoryBlockMutex );
		
		SDL_assert_release( found );
#endif

		delete pAddress;
        pAddress = nullptr;
	}
}


#ifndef _FINAL
#define gAllocate( type ) new ( #type, __FILE__, __LINE__ ) type
#define gDeallocate( object ) Genesis::MemoryAllocator::Deallocate( object )
#else
#define gAllocate( type ) new ( #type, nullptr, 0 ) type
#define gDeallocate( object ) Genesis::MemoryAllocator::Deallocate( object )
#endif

}
