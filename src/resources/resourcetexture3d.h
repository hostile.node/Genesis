// Copyright 2014 Pedro Nunes
//
// This file is part of Genesis.
//
// Genesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Genesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Genesis. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <string>

#include "../resourcemanager.h"
#include "SDL.h"

namespace Genesis
{

class ResourceTexture3d : public ResourceGeneric
{
public:
    ResourceTexture3d( const Filename& filename );
    virtual ~ResourceTexture3d(){};
    virtual ResourceType GetType() const override;
    virtual bool Load() override;

    uint32_t GetWidth() const;
    uint32_t GetHeight() const;
    uint32_t GetDepth() const;
    uint32_t GetTexture() const;

private:
    uint32_t mWidth;
    uint32_t mHeight;
    uint32_t mDepth;
    uint32_t mTextureSlot;
};

inline ResourceType ResourceTexture3d::GetType() const { return ResourceType::Texture3D; }
inline uint32_t ResourceTexture3d::GetWidth() const { return mWidth; }
inline uint32_t ResourceTexture3d::GetHeight() const { return mHeight; }
inline uint32_t ResourceTexture3d::GetDepth() const { return mDepth; }
inline uint32_t ResourceTexture3d::GetTexture() const { return mTextureSlot; }
}