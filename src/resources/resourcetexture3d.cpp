// Copyright 2014 Pedro Nunes
//
// This file is part of Genesis.
//
// Genesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Genesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Genesis. If not, see <http://www.gnu.org/licenses/>.

#include "resourcetexture3d.h"
#include "../genesis.h"
#include "../logger.h"
#include "../memory.h"
#include "sdl_image.h"
#include "rendersystem.fwd.h"

namespace Genesis
{

ResourceTexture3d::ResourceTexture3d( const Filename& filename )
    : ResourceGeneric( filename )
    , mTextureSlot( 0 )
    , mWidth( 0 )
    , mHeight( 0 )
    , mDepth( 0 )
{
}

bool ResourceTexture3d::Load()
{
    GLuint texture;
    glGenTextures( 1, &texture );
    glBindTexture( GL_TEXTURE_3D, texture );

    mWidth = 256;
    mHeight = 256;
    mDepth = 256;

    int* pData = new int[ mWidth * mHeight * mDepth ];

    int pos = 0;

    FILE* fp = 0;
    fopen_s( &fp, "data/skull.raw", "rb" );
    if ( !fp )
        return nullptr;
    unsigned char* pFileData = new unsigned char[ mWidth * mHeight * mDepth ];
    fread( pFileData, 1, mWidth * mHeight * mDepth, fp );
    fclose( fp );

    for ( unsigned int x = 0u; x < mWidth; ++x )
    {
        for ( unsigned int y = 0u; y < mHeight; ++y )
        {
            for ( unsigned int z = 0u; z < mDepth; ++z )
            {
                int v = pFileData[ pos ]; // & 0xFF;
                int a = v > 32 ? ( v << 24 ) : 0;
                pData[ pos ] = a | v | ( v << 8 ) | ( v << 16 );
                pos++;
            }
        }
    }

    delete[] pFileData;

    glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT );

    glTexImage3D( GL_TEXTURE_3D, 0, GL_RGBA8, mWidth, mHeight, mDepth, 0, GL_RGBA, GL_UNSIGNED_BYTE, pData );

    delete[] pData;

    m_State = ResourceState::Loaded;
    return true;
}
}