#pragma once

#include <string>

namespace Genesis
{

class Filename
{
public:
    Filename( const char* pFilename );
    Filename( const std::string& filename );

    const std::string& GetExtension() const;
    const std::string& GetDirectory() const;
    const std::string& GetFullPath() const;
    const std::string& GetName() const;

private:
    void Initialise( const std::string& filename );

    std::string ResolvePath( const std::string& filename ) const;
    std::string m_Extension;
    std::string m_Directory;
    std::string m_FullPath;
    std::string m_Name;
};

inline const std::string& Filename::GetExtension() const
{
    return m_Extension;
}

inline const std::string& Filename::GetDirectory() const
{
    return m_Directory;
}

inline const std::string& Filename::GetFullPath() const
{
    return m_FullPath;
}

inline const std::string& Filename::GetName() const
{
    return m_Name;
}
}