#include <fstream>

#include "memory.h"

namespace Genesis
{

MemoryBlockList MemoryAllocator::m_Blocks;
SDL_mutex* MemoryAllocator::m_pMemoryBlockMutex = nullptr;

void MemoryAllocator::Initialise()
{
#ifdef GENESIS_ENABLE_MEMORY_TRACKER
	m_pMemoryBlockMutex = SDL_CreateMutex();
#endif
}

void MemoryAllocator::Shutdown()
{

#ifdef GENESIS_ENABLE_MEMORY_TRACKER
    std::ofstream file;
    file.open( "memory.log", std::ios::out | std::ios::trunc );
    if ( file.good() )
    {
		SDL_LockMutex( m_pMemoryBlockMutex );
        file << "Memory leaks: " << m_Blocks.size() << std::endl;
        for ( auto& block : m_Blocks )
        {
            file << "[" << block.type << "] - " << block.file << " (line " << block.line << ")" << std::endl;
        }
		SDL_UnlockMutex( m_pMemoryBlockMutex );

        file.close();
    }
#endif

}

}
