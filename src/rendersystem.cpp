// Copyright 2016 Pedro Nunes
//
// This file is part of Genesis.
//
// Genesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Genesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Genesis. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <sstream>

#include <windows.h>

#include "rendersystem.fwd.h"
#include "configuration.h"
#include "genesis.h"
#include "gui/gui.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl.h"
#include "memory.h"
#include "rendersystem.h"
#include "resourcemanager.h"
#include "render/rendertarget.h"
#include "resources/resourceimage.h"
#include "resources/resourcemodel.h"
#include "scene/scene.h"
#include "shadercache.h"
#include "shaderuniform.h"
#include "videoplayer.h"

// These exports tell the drivers to use a high performance GPU.
// Without these and in computers which have an integrated graphics card, it is
// possible the game will attempt to use it rather than the dedicated GPU.
extern "C"
{
   __declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
   __declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}

namespace Genesis
{

RenderSystem::RenderSystem()
    : m_ScreenWidth( 0 )
    , m_ScreenHeight( 0 )
    , m_pPostProcessVertexBuffer( nullptr )
    , m_ScreenshotScheduled( false )
    , m_CaptureInProgress( false )
    , m_pShaderCache( nullptr )
    , m_ShaderTimer( 0.0f )
    , m_DrawCallCount( 0 )
    , m_BlendMode( BlendMode::Disabled )
    , m_ActiveScreenRenderTarget( 0 )
    , m_InputCallbackScreenshot( InputManager::sInvalidInputCallbackToken )
    , m_InputCallbackCapture( InputManager::sInvalidInputCallbackToken )
{
    // Don't do any OpenGL operations here
    // OpenGL is only available when RenderSystem::Initialize() is called
}

RenderSystem::~RenderSystem()
{
    InputManager* pInputManager = FrameWork::GetInputManager();
    if ( pInputManager != nullptr )
    {
        pInputManager->RemoveKeyboardCallback( m_InputCallbackScreenshot );
        pInputManager->RemoveKeyboardCallback( m_InputCallbackCapture );
    }

    gDeallocate( m_pShaderCache );
    gDeallocate( m_pPostProcessVertexBuffer );

    for ( auto& pPostProcessEffect : m_PostProcessVector )
    {
        gDeallocate( pPostProcessEffect );
    }

	ImGuiImpl::Shutdown();
}

void RenderSystem::Initialize( GLuint screenWidth, GLuint screenHeight )
{
	Logger* pLogger = FrameWork::GetLogger();
	GLenum err = glewInit();
	if ( err == GLEW_OK )
	{
		pLogger->LogInfo( "%s", "GLEW initialised." );
	}
	else
	{
		pLogger->LogError( "GLEW initialisation failed: %s", glewGetErrorString(err) );
		return;
	}

    m_ScreenWidth = screenWidth;
    m_ScreenHeight = screenHeight;

    // Initialize everything that might be needed for debugging purposes
    InitializeDebug();

	// Set OpenGL version to 3.3.
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE ); // OpenGL core profile
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 ); // OpenGL 3+
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 3 ); // OpenGL 3.3

    // Log graphics card information
    pLogger->LogInfo( "Graphics card info:" );
    pLogger->LogInfo( " - Vendor: %s", glGetString( GL_VENDOR ) );
    pLogger->LogInfo( " - Renderer: %s", glGetString( GL_RENDERER ) );
    pLogger->LogInfo( " - OpenGL version: %s", glGetString( GL_VERSION ) );
    pLogger->LogInfo( " - Shader model info: %s", glGetString( GL_SHADING_LANGUAGE_VERSION ) );

	int major, minor;
	SDL_GL_GetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, &major );
	SDL_GL_GetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, &minor );
	pLogger->LogInfo( "Using OpenGL version %d.%d.", major, minor );

    m_ProjectionMatrix = glm::perspective( 45.0f, static_cast<float>( m_ScreenWidth ) / static_cast<float>( m_ScreenHeight ), 1.0f, 1000.0f );
    m_ViewMatrix = glm::mat4( 1.0f );

    m_pShaderCache = gAllocate( ShaderCache );

    m_pPostProcessVertexBuffer = gAllocate( VertexBuffer )( GeometryType::Quad, VBO_POSITION | VBO_UV );
	CreateRenderTargets();
    InitializePostProcessing();

    m_InputCallbackScreenshot = FrameWork::GetInputManager()->AddKeyboardCallback( std::bind( &RenderSystem::TakeScreenshot, this ), SDL_SCANCODE_PRINTSCREEN, ButtonState::Pressed );
    m_InputCallbackCapture = FrameWork::GetInputManager()->AddKeyboardCallback( std::bind( &RenderSystem::Capture, this ), SDL_SCANCODE_F8, ButtonState::Pressed );
}

void RenderSystem::CreateRenderTargets()
{
	m_ScreenRenderTarget[ 0 ] = RenderTarget::Create( "Internal fullscreen A", m_ScreenWidth, m_ScreenHeight, true, true );
	m_ScreenRenderTarget[ 1 ] = RenderTarget::Create( "Internal fullscreen B", m_ScreenWidth, m_ScreenHeight, true, true );
	m_GlowRenderTarget = RenderTarget::Create( "Glow", m_ScreenWidth / 2, m_ScreenHeight / 2, false, false );
	m_RadarRenderTarget = RenderTarget::Create( "Radar", 256, 256, false, false );
}

void RenderSystem::InitializeDebug()
{
    mInternalFormatMap[ GL_STENCIL_INDEX ] = "GL_STENCIL_INDEX";
    mInternalFormatMap[ GL_DEPTH_COMPONENT ] = "GL_DEPTH_COMPONENT";
    mInternalFormatMap[ GL_ALPHA ] = "GL_ALPHA";
    mInternalFormatMap[ GL_RGB ] = "GL_RGB";
    mInternalFormatMap[ GL_RGBA ] = "GL_RGBA";
    mInternalFormatMap[ GL_LUMINANCE ] = "GL_LUMINANCE";
    mInternalFormatMap[ GL_LUMINANCE_ALPHA ] = "GL_LUMINANCE_ALPHA";
    mInternalFormatMap[ GL_ALPHA4 ] = "GL_ALPHA4";
    mInternalFormatMap[ GL_ALPHA8 ] = "GL_ALPHA8";
    mInternalFormatMap[ GL_ALPHA12 ] = "GL_ALPHA12";
    mInternalFormatMap[ GL_ALPHA16 ] = "GL_ALPHA16";
    mInternalFormatMap[ GL_LUMINANCE4 ] = "GL_LUMINANCE4";
    mInternalFormatMap[ GL_LUMINANCE8 ] = "GL_LUMINANCE8";
    mInternalFormatMap[ GL_LUMINANCE16 ] = "GL_LUMINANCE16";
    mInternalFormatMap[ GL_LUMINANCE4_ALPHA4 ] = "GL_LUMINANCE4_ALPHA4";
    mInternalFormatMap[ GL_LUMINANCE6_ALPHA2 ] = "GL_LUMINANCE6_ALPHA2";
    mInternalFormatMap[ GL_LUMINANCE8_ALPHA8 ] = "GL_LUMINANCE8_ALPHA8";
    mInternalFormatMap[ GL_LUMINANCE12_ALPHA4 ] = "GL_LUMINANCE12_ALPHA4";
    mInternalFormatMap[ GL_LUMINANCE12_ALPHA12 ] = "GL_LUMINANCE12_ALPHA12";
    mInternalFormatMap[ GL_LUMINANCE16_ALPHA16 ] = "GL_LUMINANCE16_ALPHA16";
    mInternalFormatMap[ GL_INTENSITY ] = "GL_INTENSITY";
    mInternalFormatMap[ GL_INTENSITY4 ] = "GL_INTENSITY4";
    mInternalFormatMap[ GL_INTENSITY8 ] = "GL_INTENSITY8";
    mInternalFormatMap[ GL_INTENSITY12 ] = "GL_INTENSITY12";
    mInternalFormatMap[ GL_INTENSITY16 ] = "GL_INTENSITY16";
    mInternalFormatMap[ GL_R3_G3_B2 ] = "GL_R3_G3_B2";
    mInternalFormatMap[ GL_RGB4 ] = "GL_RGB4";
    mInternalFormatMap[ GL_RGB5 ] = "GL_RGB5";
    mInternalFormatMap[ GL_RGB8 ] = "GL_RGB8";
    mInternalFormatMap[ GL_RGB10 ] = "GL_RGB10";
    mInternalFormatMap[ GL_RGB12 ] = "GL_RGB12";
    mInternalFormatMap[ GL_RGB16 ] = "GL_RGB16";
    mInternalFormatMap[ GL_RGBA2 ] = "GL_RGBA2";
    mInternalFormatMap[ GL_RGBA4 ] = "GL_RGBA4";
    mInternalFormatMap[ GL_RGB5_A1 ] = "GL_RGB5_A1";
    mInternalFormatMap[ GL_RGBA8 ] = "GL_RGBA8";
    mInternalFormatMap[ GL_RGB10_A2 ] = "GL_RGB10_A2";
    mInternalFormatMap[ GL_RGBA12 ] = "GL_RGBA12";
    mInternalFormatMap[ GL_RGBA16 ] = "GL_RGBA16";
}

RenderTarget* RenderSystem::GetRenderTarget( RenderTargetId id )
{
	RenderTarget* pRenderTarget = nullptr;
    if ( id == RenderTargetId::Default )
	{
        pRenderTarget = m_ScreenRenderTarget[ m_ActiveScreenRenderTarget ].get();
	}
    else if ( id == RenderTargetId::Glow )
	{
        pRenderTarget = m_GlowRenderTarget.get();
	}
    else if ( id == RenderTargetId::Radar )
	{
        pRenderTarget = m_RadarRenderTarget.get();
	}

    return pRenderTarget;
}

void RenderSystem::InitializePostProcessing()
{
	if ( Configuration::IsPostProcessingEnabled() )
	{
		CreatePostProcessStep( "postprocess_glow", RenderTargetId::Glow, RenderTargetId::Default, BlendMode::Add );
		CreatePostProcessStep( "postprocess_bleachbypass", RenderTargetId::Default, RenderTargetId::Default, BlendMode::Disabled );
	}
	else
	{
		CreatePostProcessStep( "postprocess_diffuse", RenderTargetId::Default, RenderTargetId::Default, BlendMode::Disabled );
	}
}

void RenderSystem::CreatePostProcessStep( const std::string& shaderName, RenderTargetId sourceId, RenderTargetId targetId, BlendMode blendMode )
{
    PostProcessEffect* postProcessEffect = gAllocate( PostProcessEffect );
    postProcessEffect->pShader = GetShaderCache()->Load( shaderName );

    if ( sourceId == targetId )
    {
        postProcessEffect->pSource = m_ScreenRenderTarget[ m_ActiveScreenRenderTarget ].get();
        m_ActiveScreenRenderTarget = ( m_ActiveScreenRenderTarget == 0u ) ? 1u : 0u;
        postProcessEffect->pTarget = m_ScreenRenderTarget[ m_ActiveScreenRenderTarget ].get();
    }
    else
    {
        postProcessEffect->pSource = GetRenderTarget( sourceId );
        postProcessEffect->pTarget = GetRenderTarget( targetId );
    }
    postProcessEffect->blendMode = blendMode;

    ShaderUniform* pSampler = postProcessEffect->pShader->RegisterUniform( "k_sampler0", ShaderUniformType::Texture );
    pSampler->Set( postProcessEffect->pSource->GetColor(), GL_TEXTURE0 );

    ShaderUniform* pResolution = postProcessEffect->pShader->RegisterUniform( "k_sourceResolution", ShaderUniformType::FloatVector2 );
    if ( pResolution != nullptr )
    {
        pResolution->Set( glm::vec2( (float)postProcessEffect->pSource->GetWidth(), (float)postProcessEffect->pSource->GetHeight() ) );
    }

    m_PostProcessVector.push_back( postProcessEffect );
}

std::string RenderSystem::ConvertInternalFormatToString( GLenum format )
{
    InternalFormatMap::iterator it = mInternalFormatMap.find( format );
    if ( it != mInternalFormatMap.end() )
        return it->second;
    return "Unknown";
}

std::string RenderSystem::GetRenderbufferParameters( GLuint id )
{
    if ( glIsRenderbufferEXT( id ) == GL_FALSE )
        return "Not Renderbuffer object";

    int width, height, format;
    std::string formatName;
    glBindRenderbufferEXT( GL_RENDERBUFFER_EXT, id );
    glGetRenderbufferParameterivEXT( GL_RENDERBUFFER_EXT, GL_RENDERBUFFER_WIDTH_EXT, &width ); // get renderbuffer width
    glGetRenderbufferParameterivEXT( GL_RENDERBUFFER_EXT, GL_RENDERBUFFER_HEIGHT_EXT, &height ); // get renderbuffer height
    glGetRenderbufferParameterivEXT( GL_RENDERBUFFER_EXT, GL_RENDERBUFFER_INTERNAL_FORMAT_EXT, &format ); // get renderbuffer internal format
    glBindRenderbufferEXT( GL_RENDERBUFFER_EXT, 0 );

    formatName = ConvertInternalFormatToString( format );

    std::stringstream ss;
    ss << width << "x" << height << ", " << formatName;
    return ss.str();
}

std::string RenderSystem::GetTextureParameters( GLuint id )
{
    if ( glIsTexture( id ) == GL_FALSE )
        return "Not texture object";

    int width, height, format;
    glBindTexture( GL_TEXTURE_2D, id );
    glGetTexLevelParameteriv( GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width ); // get texture width
    glGetTexLevelParameteriv( GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height ); // get texture height
    glGetTexLevelParameteriv( GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &format ); // get texture internal format
    glBindTexture( GL_TEXTURE_2D, 0 );

    std::stringstream ss;
    ss << width << "x" << height << ", " << ConvertInternalFormatToString( format );
    return ss.str();
}

void RenderSystem::PrintFramebufferInfo( GLuint fboId )
{
	glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, fboId );

	Logger* pLogger = FrameWork::GetLogger();

    // print max # of colorbuffers supported by FBO
    int colorBufferCount = 0;
    glGetIntegerv( GL_MAX_COLOR_ATTACHMENTS_EXT, &colorBufferCount );
    pLogger->LogInfo( "> Max Number of Color Buffer Attachment Points: %d.", colorBufferCount );

    int objectType;
    int objectId;

    // print info of the colorbuffer attachable image
    for ( int i = 0; i < colorBufferCount; ++i )
    {
        glGetFramebufferAttachmentParameterivEXT( GL_FRAMEBUFFER_EXT,
            GL_COLOR_ATTACHMENT0_EXT + i,
            GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_EXT,
            &objectType );
        if ( objectType != GL_NONE )
        {
            glGetFramebufferAttachmentParameterivEXT( GL_FRAMEBUFFER_EXT,
                GL_COLOR_ATTACHMENT0_EXT + i,
                GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_EXT,
                &objectId );

			std::stringstream output;
			output << "> Color Attachment " << i << ": ";
            if ( objectType == GL_TEXTURE )
				output << "GL_TEXTURE, " << GetTextureParameters( objectId );
            else if ( objectType == GL_RENDERBUFFER_EXT )
				output << "GL_RENDERBUFFER_EXT, " << GetRenderbufferParameters( objectId );
			pLogger->LogInfo( "%s", output.str().c_str() );
        }
    }

    // print info of the depthbuffer attachable image
    glGetFramebufferAttachmentParameterivEXT( GL_FRAMEBUFFER_EXT,
        GL_DEPTH_ATTACHMENT_EXT,
        GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_EXT,
        &objectType );
    if ( objectType != GL_NONE )
    {
        glGetFramebufferAttachmentParameterivEXT( GL_FRAMEBUFFER_EXT,
            GL_DEPTH_ATTACHMENT_EXT,
            GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_EXT,
            &objectId );

		std::stringstream output;
		output << "> Depth Attachment: ";
        switch ( objectType )
        {
        case GL_TEXTURE:
            output << "GL_TEXTURE, " << GetTextureParameters( objectId );
            break;
        case GL_RENDERBUFFER_EXT:
            output << "GL_RENDERBUFFER_EXT, " << GetRenderbufferParameters( objectId );
            break;
        }
		pLogger->LogInfo( "%s", output.str().c_str() );
    }

    // print info of the stencilbuffer attachable image
    glGetFramebufferAttachmentParameterivEXT( GL_FRAMEBUFFER_EXT,
        GL_STENCIL_ATTACHMENT_EXT,
        GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_EXT,
        &objectType );
    if ( objectType != GL_NONE )
    {
        glGetFramebufferAttachmentParameterivEXT( GL_FRAMEBUFFER_EXT,
            GL_STENCIL_ATTACHMENT_EXT,
            GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_EXT,
            &objectId );

		std::stringstream output;
        output << "> Stencil Attachment: ";
        switch ( objectType )
        {
        case GL_TEXTURE:
            output << "GL_TEXTURE, " << GetTextureParameters( objectId );
            break;
        case GL_RENDERBUFFER_EXT:
            output << "GL_RENDERBUFFER_EXT, " << GetRenderbufferParameters( objectId );
            break;
        }
		pLogger->LogInfo( "%s", output.str().c_str() );
    }

	glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, 0 );
}

void RenderSystem::SetRenderTarget( RenderTargetId renderTargetId )
{
    SetRenderTarget( GetRenderTarget( renderTargetId ) );
}

void RenderSystem::SetRenderTarget( RenderTarget* pRenderTarget )
{
	if ( pRenderTarget != nullptr )
	{
		glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, pRenderTarget->GetFBO() );
		glViewport( 0, 0, pRenderTarget->GetWidth(), pRenderTarget->GetHeight() );
	}
	else
	{
		glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, 0 );
	}
}

void RenderSystem::RenderScene()
{
    Scene* scene = FrameWork::GetScene();

    m_ScreenRenderTarget[ 0 ]->Clear();
    m_ScreenRenderTarget[ 1 ]->Clear();
    m_RadarRenderTarget->Clear();
	m_GlowRenderTarget->Clear();

	SetBlendMode( BlendMode::Disabled );
    SetRenderTarget( RenderTargetId::Default );
    scene->Render();
    SetRenderTarget( RenderTargetId::None );
}

TaskStatus RenderSystem::Update( float delta )
{
    ResetDrawCallCount();

    m_ShaderTimer += delta;

    m_ActiveScreenRenderTarget = 0;

    ViewPerspective();
    RenderScene();
    ViewOrtho();

    unsigned int numPostProcessSteps = m_PostProcessVector.size();
    for ( unsigned int i = 0; i < numPostProcessSteps; i++ )
    {
        const bool renderToTexture = ( i < numPostProcessSteps - 1 );
        if ( renderToTexture )
        {
            glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, m_PostProcessVector[ i ]->pTarget->GetFBO() );
        }

        SetBlendMode( m_PostProcessVector[ i ]->blendMode );

        const float width = static_cast<float>( m_PostProcessVector[ i ]->pTarget->GetWidth() );
        const float height = static_cast<float>( m_PostProcessVector[ i ]->pTarget->GetHeight() );
        m_pPostProcessVertexBuffer->CreateTexturedQuad( 0.0f, 0.0f, width, height );
        m_PostProcessVector[ i ]->pShader->Use();
        m_pPostProcessVertexBuffer->Draw();

        if ( renderToTexture )
        {
            glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, 0 );
        }
    }

    SetBlendMode( BlendMode::Disabled );

    FrameWork::GetGuiManager()->Render( delta );
	ImGui::Render();
    ImGuiImpl::Render();

    FrameWork::GetWindow()->Present();

    if ( IsScreenshotScheduled() )
    {
        TakeScreenshotAux( true );
    }

    return TaskStatus::Continue;
}

void RenderSystem::ViewOrtho()
{
    m_ViewMatrix = glm::mat4();
    m_ProjectionMatrix = glm::ortho( 0.0f, static_cast<float>( m_ScreenWidth ), static_cast<float>( m_ScreenHeight ), 0.0f, -1.0f, 1.0f );
}

void RenderSystem::ViewPerspective()
{
    Scene* scene = FrameWork::GetScene();
    Camera* camera = scene->GetCamera();
    glm::vec3 cPos = camera->GetPosition();
    glm::vec3 cTgt = camera->GetTargetPosition();

    m_ViewMatrix = glm::lookAt(
        glm::vec3( cPos.x, cPos.y, cPos.z ),
        glm::vec3( cTgt.x, cTgt.y, cTgt.z ),
        glm::vec3( 0.0f, 1.0f, 0.0f ) );

    m_ProjectionMatrix = glm::perspective( 45.0f, static_cast<float>( m_ScreenWidth ) / static_cast<float>( m_ScreenHeight ), 1.0f, 1000.0f );
}

IntersectionResult RenderSystem::LinePlaneIntersection( const glm::vec3& position, const glm::vec3& direction, const glm::vec3& planePosition, const glm::vec3& planeNormal, glm::vec3& result )
{
    const glm::vec3 u = direction;
    const glm::vec3 w = position - planePosition;

    const float D = glm::dot( planeNormal, direction );
    const float N = -glm::dot( planeNormal, w );

    if ( fabs( D ) < 0.00001f ) // segment is parallel to plane
    {
        if ( N == 0 ) // segment lies in plane
        {
            return IntersectionResult::FailureCoplanar;
        }
        else // no intersection
        {
            return IntersectionResult::FailureParallel;
        }
    }

    // they are not parallel
    // compute intersect param
    const float sI = N / D;
    result = position + sI * u; // compute segment intersect point
    return IntersectionResult::Success;
}

void RenderSystem::ScreenPosToWorldRay(
    int mouseX, int mouseY, // Mouse position, in pixels, from bottom-left corner of the window
    int screenWidth, int screenHeight, // Window size, in pixels
    const glm::mat4& ViewMatrix, // Camera position and orientation
    const glm::mat4& ProjectionMatrix, // Camera parameters (ratio, field of view, near and far planes)
    glm::vec3& out_origin, // Ouput : Origin of the ray. /!\ Starts at the near plane, so if you want the ray to start at the camera's position instead, ignore this.
    glm::vec3& out_direction // Ouput : Direction, in world space, of the ray that goes "through" the mouse.
    )
{
    // The ray Start and End positions, in Normalized Device Coordinates
    glm::vec4 lRayStart_NDC(
        ( (float)mouseX / (float)screenWidth - 0.5f ) * 2.0f, // [0,1024] -> [-1,1]
        ( (float)mouseY / (float)screenHeight - 0.5f ) * 2.0f, // [0, 768] -> [-1,1]
        -1.0, // The near plane maps to Z=-1 in Normalized Device Coordinates
        1.0f );
    glm::vec4 lRayEnd_NDC(
        ( (float)mouseX / (float)screenWidth - 0.5f ) * 2.0f,
        ( (float)mouseY / (float)screenHeight - 0.5f ) * 2.0f,
        0.0,
        1.0f );

    glm::mat4 M = glm::inverse( ProjectionMatrix * ViewMatrix );
    glm::vec4 lRayStart_world = M * lRayStart_NDC;
    lRayStart_world /= lRayStart_world.w;
    glm::vec4 lRayEnd_world = M * lRayEnd_NDC;
    lRayEnd_world /= lRayEnd_world.w;

    glm::vec3 lRayDir_world( lRayEnd_world - lRayStart_world );
    lRayDir_world = glm::normalize( lRayDir_world );

    out_origin = glm::vec3( lRayStart_world );
    out_direction = glm::normalize( lRayDir_world );
}

glm::vec3 RenderSystem::Raycast( const glm::vec2& screenCoordinates )
{
    glm::vec3 origin, direction;
    ScreenPosToWorldRay( (int)screenCoordinates.x, m_ScreenHeight - (int)screenCoordinates.y, m_ScreenWidth, m_ScreenHeight, m_ViewMatrix, m_ProjectionMatrix, origin, direction );

    // Intersection with the Z plane
    const glm::vec3 planePosition( 0.0f, 0.0f, 0.0f );
    const glm::vec3 planeNormal( 0.0f, 0.0f, 1.0f );

    glm::vec3 result;
    IntersectionResult intersectionResult = LinePlaneIntersection( origin, direction, planePosition, planeNormal, result );
    SDL_assert( intersectionResult == IntersectionResult::Success );

    return result;
}

void RenderSystem::TakeScreenshot()
{
    InputManager* pInputManager = FrameWork::GetInputManager();
    const bool immediateScreenshot = !( pInputManager->IsButtonPressed( SDL_SCANCODE_LCTRL ) || pInputManager->IsButtonPressed( SDL_SCANCODE_RCTRL ) );
    TakeScreenshotAux( immediateScreenshot );
}

void RenderSystem::TakeScreenshotAux( bool immediate )
{
    if ( immediate )
    {
        std::string filename;
        if ( GetScreenshotFilename( filename ) )
        {
            SDL_Surface* pSurface = SDL_CreateRGBSurface( SDL_SWSURFACE, m_ScreenWidth, m_ScreenHeight, 24, 0x000000FF, 0x0000FF00, 0x00FF0000, 0 );
            glReadPixels( 0, 0, m_ScreenWidth, m_ScreenHeight, GL_RGB, GL_UNSIGNED_BYTE, pSurface->pixels );

            // The image needs to be flipped vertically before being dumped into the BMP
            SDL_Surface* pFlippedSurface = FlipSurfaceVertical( pSurface );
            SDL_SaveBMP( pFlippedSurface, filename.c_str() );
            SDL_FreeSurface( pSurface );
            SDL_FreeSurface( pFlippedSurface );

            FrameWork::GetLogger()->LogInfo( "Screenshot taken: %s", filename.c_str() );
        }
        else
        {
            FrameWork::GetLogger()->LogWarning( "Couldn't take screenshot." );
        }

        m_ScreenshotScheduled = false;
    }
    else
    {
        FrameWork::GetLogger()->LogInfo( "Screenshot scheduled..." );
        m_ScreenshotScheduled = true;
    }
}

void RenderSystem::Capture()
{
    if ( IsCaptureInProgress() )
    {
        EndCapture();
    }
    else
    {
        BeginCapture();
    }
}

// Function courtesy of Sebastian Beschke s.beschke@gmx.de, adapted to C
// From http://lists.libsdl.org/pipermail/sdl-libsdl.org/2005-January/047963.html
SDL_Surface* RenderSystem::FlipSurfaceVertical( SDL_Surface* pSurface ) const
{
    SDL_Surface* pFlippedSurface = SDL_CreateRGBSurface(
        pSurface->flags,
        pSurface->w, pSurface->h,
        pSurface->format->BytesPerPixel * 8,
        pSurface->format->Rmask, pSurface->format->Gmask, pSurface->format->Bmask, pSurface->format->Amask );
    Uint8* pixels = (Uint8*)pSurface->pixels;
    Uint8* rpixels = (Uint8*)pFlippedSurface->pixels;
    Uint32 pitch = pSurface->pitch;
    Uint32 pxlength = pitch * pSurface->h;

    for ( Uint32 line = 0; line < (Uint32)pSurface->h; ++line )
    {
        Uint32 pos = line * pitch;
        memcpy( &rpixels[ pxlength - pos - pitch ], &pixels[ pos ], pitch );
    }

    return pFlippedSurface;
}

bool RenderSystem::GetScreenshotFilename( std::string& filename ) const
{
    for ( int counter = 1; counter <= 999; ++counter )
    {
        std::stringstream ss;
        ss << "Screenshot" << counter << ".bmp";
        std::string temporaryFilename = ss.str();

        std::ifstream file( temporaryFilename.c_str() );
        if ( file.good() )
        {
            file.close();
        }
        else
        {
            file.close();
            filename = temporaryFilename;
            return true;
        }
    }

    return false;
}

void RenderSystem::SetBlendMode( BlendMode blendMode )
{
    if ( GetBlendMode() == blendMode )
    {
        return;
    }
    else if ( blendMode == BlendMode::Disabled )
    {
        glDisable( GL_BLEND );
        m_BlendMode = BlendMode::Disabled;
        return;
    }
    else if ( GetBlendMode() == BlendMode::Disabled )
    {
        glEnable( GL_BLEND );
    }

    if ( blendMode == BlendMode::Blend )
    {
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        m_BlendMode = BlendMode::Blend;
    }
    else if ( blendMode == BlendMode::Add )
    {
        glBlendFunc( GL_SRC_ALPHA, GL_ONE );
        m_BlendMode = BlendMode::Add;
    }
    else if ( blendMode == BlendMode::Screen )
    {
        glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_COLOR );
        m_BlendMode = BlendMode::Screen;
    }
    else if ( blendMode == BlendMode::Multiply )
    {
        glBlendFunc( GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA );
        m_BlendMode = BlendMode::Multiply;
    }
}
}