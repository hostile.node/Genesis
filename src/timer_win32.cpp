// Copyright 2017 Pedro Nunes
//
// This file is part of Genesis.
//
// Genesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Genesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Genesis. If not, see <http://www.gnu.org/licenses/>.

#include "timer.h"

namespace Genesis
{

// Visual Studio 2012 doesn't support std::chrono:high_resolution_clock properly - the maximum
// resolution is about 1ms, which isn't sufficient for our needs.
// So we implement an alternative variant for Windows, using a QPC:
// https://msdn.microsoft.com/en-us/library/windows/desktop/dn553408%28v=vs.85%29.aspx

Timer::Timer()
    : m_Delta( 0.0f )
{
    QueryPerformanceCounter( &m_TimePoint );
}

void Timer::Update()
{
    LARGE_INTEGER frequency, now, delta;
    QueryPerformanceFrequency( &frequency );
    QueryPerformanceCounter( &now );
    delta.QuadPart = now.QuadPart - m_TimePoint.QuadPart;
    delta.QuadPart *= 1000000;
    delta.QuadPart /= frequency.QuadPart;
    m_Delta = static_cast<float>( delta.QuadPart ) / 1000000.0f;
    m_TimePoint = now;
}
}