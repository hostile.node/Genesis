// Copyright 2014 Pedro Nunes
//
// This file is part of Genesis.
//
// Genesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Genesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Genesis. If not, see <http://www.gnu.org/licenses/>.

#include "genesis.h"
#include "eventhandler.h"
#include "imgui/imgui.h"
#include "gui/gui.h"
#include "inputmanager.h"
#include "memory.h"
#include "render/debugrender.h"
#include "rendersystem.h"
#include "resourcemanager.h"
#include "scene/scene.h"
#include "soundmanager.h"
#include "taskmanager.h"
#include "timer.h"
#include "videoplayer.h"
#include "window.h"

namespace Genesis
{

//---------------------------------------------------------------
// Globals
//---------------------------------------------------------------

TaskManager* gTaskManager = nullptr;
Logger* gLogger = nullptr;
InputManager* gInputManager = nullptr;
EventHandler* gEventHandler = nullptr;
Window* gWindow = nullptr;
RenderSystem* gRenderSystem = nullptr;
ResourceManager* gResourceManager = nullptr;
Scene* gScene = nullptr;
Gui::GuiManager* gGuiManager = nullptr;
SoundManager* gSoundManager = nullptr;
VideoPlayer* gVideoPlayer = nullptr;
Render::DebugRender* gDebugRender = nullptr;

CommandLineParameters* FrameWork::m_pCommandLineParameters = nullptr;

//-------------------------------------------------------------------
// FrameWork
//-------------------------------------------------------------------

bool FrameWork::Initialize()
{
    // Initialize the Logger
    // We also create a FileLogger to start logging to "log.txt" and
    // a MessageBoxLogger that creates a message box if the log is
    // a warning or an error.
    gLogger = gAllocate( Logger );

    gLogger->AddLogTarget( gAllocate( FileLogger )( "log.txt" ) );
    gLogger->AddLogTarget( gAllocate( MessageBoxLogger ) );
#ifdef WIN32
#ifndef _FINAL
    gLogger->AddLogTarget( gAllocate( VisualStudioLogger ) );
#endif
#endif

    // Initialize SDL
    // Needs to be done before InputManager() is created,
    // otherwise key repetition won't work.
    if ( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS ) < 0 )
    {
        gLogger->LogError( "%s", SDL_GetError() );
    }

    gInputManager = gAllocate( InputManager );
    gEventHandler = gAllocate( EventHandler );
    gResourceManager = gAllocate( ResourceManager );

    // Initialize the task manager, as well as all the related tasks
    gTaskManager = gAllocate( TaskManager )( gLogger );

    gTaskManager->AddTask( "InputManager", gInputManager, (TaskFunc)&InputManager::Update, TaskPriority::System );
    gTaskManager->AddTask( "EventHandler", gEventHandler, (TaskFunc)&EventHandler::Update, TaskPriority::System );

    gRenderSystem = gAllocate( RenderSystem );
    gTaskManager->AddTask( "Render", gRenderSystem, (TaskFunc)&RenderSystem::Update, TaskPriority::Rendering );

    gGuiManager = gAllocate( Gui::GuiManager );
    gTaskManager->AddTask( "GUIManager", gGuiManager, (TaskFunc)&Gui::GuiManager::Update, TaskPriority::GameLogic );

    gScene = gAllocate( Scene );
    gTaskManager->AddTask( "Scene", gScene, (TaskFunc)&Scene::Update, TaskPriority::GameLogic );

    gSoundManager = gAllocate( SoundManager );
    gTaskManager->AddTask( "SoundManager", gSoundManager, (TaskFunc)&SoundManager::Update, TaskPriority::System );

    return true;
}

void FrameWork::Shutdown()
{
	gDeallocate( gDebugRender );
    gDeallocate( gGuiManager );
    gDeallocate( gVideoPlayer );
    gDeallocate( gInputManager );
    gDeallocate( gEventHandler );
    gDeallocate( gTaskManager );
    gDeallocate( gRenderSystem );
    gDeallocate( gResourceManager );
    gDeallocate( gSoundManager );
    gDeallocate( gScene );
    gDeallocate( gWindow );
    gDeallocate( gLogger );

    MemoryAllocator::Shutdown();
}

bool FrameWork::CreateWindowGL( const std::string& name, uint32_t width, uint32_t height, uint32_t multiSampleSamples /* = 0 */ )
{
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
    SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8 );

    if ( multiSampleSamples > 0 )
    {
        SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS, 1 );
        SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, multiSampleSamples );
    }

	gWindow = gAllocate( Window )( name, width, height, Configuration::IsFullscreen() );

    InitializeOpenGL();
    GetRenderSystem()->Initialize( width, height );
    GetGuiManager()->Initialize();
	ImGuiImpl::Initialise();

    gVideoPlayer = gAllocate( VideoPlayer );
    gTaskManager->AddTask( "VideoPlayer", gVideoPlayer, (TaskFunc)&VideoPlayer::Update, TaskPriority::System );

	gDebugRender = gAllocate( Render::DebugRender );

    return true;
}

void FrameWork::InitializeOpenGL()
{
    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    glClearDepth( 1.0f );
    glDepthFunc( GL_LEQUAL );
    glEnable( GL_DEPTH_TEST );
    glShadeModel( GL_SMOOTH );
    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
    glEnable( GL_TEXTURE_2D );
}

CommandLineParameters* FrameWork::CreateCommandLineParameters( const char* parameterStr )
{
    m_pCommandLineParameters = gAllocate( CommandLineParameters )( parameterStr );
    return m_pCommandLineParameters;
}

CommandLineParameters* FrameWork::CreateCommandLineParameters( const char** parameters, uint32_t numParameters )
{
    m_pCommandLineParameters = gAllocate( CommandLineParameters )( parameters, numParameters );
    return m_pCommandLineParameters;
}

CommandLineParameters* FrameWork::GetCommandLineParameters()
{
    return m_pCommandLineParameters;
}

Logger* FrameWork::GetLogger()
{
    return gLogger;
}

TaskManager* FrameWork::GetTaskManager()
{
    return gTaskManager;
}

InputManager* FrameWork::GetInputManager()
{
    return gInputManager;
}

Window* FrameWork::GetWindow()
{
    return gWindow;
}

RenderSystem* FrameWork::GetRenderSystem()
{
    return gRenderSystem;
}

ResourceManager* FrameWork::GetResourceManager()
{
    return gResourceManager;
}

Scene* FrameWork::GetScene()
{
    return gScene;
}

Gui::GuiManager* FrameWork::GetGuiManager()
{
    return gGuiManager;
}

SoundManager* FrameWork::GetSoundManager()
{
    return gSoundManager;
}

VideoPlayer* FrameWork::GetVideoPlayer()
{
    return gVideoPlayer;
}

Render::DebugRender* FrameWork::GetDebugRender()
{
	return gDebugRender;
}

//---------------------------------------------------------------
// CommandLineParameters
//---------------------------------------------------------------

CommandLineParameters::CommandLineParameters( const char* parameterStr )
{
    // Do we even have any parameters?
    if ( parameterStr != nullptr )
    {
        std::string tmpStr( parameterStr );

        uint32_t previousPos = 0;
        uint32_t currentPos = tmpStr.find_first_of( " ", 0 );
        // If there is no whitespace, then there's only one parameter
        if ( currentPos == std::string::npos )
        {
            mParameters.push_back( parameterStr );
        }
        // Otherwise, process every parameter
        else
        {
            do
            {
                mParameters.push_back( tmpStr.substr( previousPos, currentPos - previousPos ) );
                previousPos = currentPos + 1;
                currentPos = tmpStr.find_first_of( " ", previousPos );
            } while ( currentPos != std::string::npos );

            mParameters.push_back( tmpStr.substr( previousPos, tmpStr.size() - previousPos ) );
        }
    }
}

CommandLineParameters::CommandLineParameters( const char** parameters, uint32_t numParameters )
{
    for ( uint32_t i = 0; i < numParameters; i++ )
    {
        mParameters.push_back( parameters[ i ] );
    }
}

bool CommandLineParameters::HasParameter( const std::string& name ) const
{
    for ( auto& parameter : mParameters )
    {
        if ( parameter == name )
        {
            return true;
        }
    }

    return false;
}
}