function customAddFileConfig(base, fcfg, cfg)

	-- call the base method to add the file config
	base(fcfg, cfg)

	-- get the file configuration object
	local config = premake.fileconfig.getconfig(fcfg, cfg)

	-- exclude all source files from the build other than the unity and resource files.
	local filename = fcfg.relpath
	local basename = path.getbasename(filename)
	local extension = path.getextension(filename)
	if basename ~= "unity" and extension == ".cpp" then
		config.flags.ExcludeFromBuild = true
	end
end

project "Genesis"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"
	location ".build-premake"
	floatingpoint "Fast"
	staticruntime "On"
	systemversion "latest"
	warnings "Extra"

	files { "src/**.h", "src/**.hpp", "src/**.inl", "src/**.cpp" }

	includedirs {
		"src",
		"libs/SDL2-2.0.5/include",
		"libs/SDL2_image-2.0.1/include",
		"libs/fmodex/include",
		"libs/libvpx-v1.10.0/include",
		"libs/bullet3-2.87/include",
		"libs/glew-2.1.0/include"
	}
	
	flags {
		"FatalWarnings",
		"NoPCH"
	}
	
	filter "platforms:Win32"
		defines {
			"WIN32",
			"_WINDOWS",
			"_HAS_EXCEPTIONS=0"	
		}
		disablewarnings {
			"4100", -- unreferenced formal parameter
			"4121", -- alignment of a member was sensitive to packing
			"4127", -- conditional expression is constant
			"4189", -- local variable is initialized but not referenced
			"4201", -- nonstandard extension used : nameless struct/union
			"4702"  -- unreachable code
		}

	filter "configurations:debug"
		targetdir "libs/Genesis/debug"
		defines {
			"_DEBUG"
		}
		symbols "On"

	filter "configurations:release"
		targetdir "libs/Genesis/release"
		defines {
			"_RELEASE",
			"NDEBUG"
		}
		symbols "On"
		optimize "On"
		
	filter "configurations:final"
		targetdir "libs/Genesis/final"
		defines {
			"_RELEASE",
			"NDEBUG",
			"_FINAL"
		}
		flags {
			"LinkTimeOptimization"
		}
		optimize "Full"

	-- Reset the filter for other settings
	filter { }

	premake.override(premake.fileconfig, "addconfig", customAddFileConfig)